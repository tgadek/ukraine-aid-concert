<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ukraine Aid Concert</title>
    <link rel = "icon" href = "logo_concert_mini.png" type = "image/x-icon">
    <style type="text/css">
    h1 {text-align: center; font-family: Helvetica, sans-serif; font-size: 24px;}
    
    A {text-decoration: none; color: white; font-weight: bold;}
    a {text-decoration: none; color: white; font-weight: bold;}
    div {text-align: center; align: center; font-family: Helvetica, sans-serif; font-size: 18px; width: 60%; align-items: center; justify-content: center; margin: auto;}
    .div2 {text-align: right; align: center; font-family: Helvetica, sans-serif; font-size: 18px; width: 60%; align-items: center; justify-content: center; margin: auto;}
     p {text-align: right;}
    info {text-align: center; font-family: Helvetica, sans-serif; font-size: 15px;}
    foot {text-align: right; font-family:"Helvetica", Helvetica, sans-serif; font-size:10px;}
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{border-color:black;border-style:solid;border-width:0px;font-family:Helvetica, sans-serif;font-size:18px;
      overflow:hidden;padding:5px 5px;word-break:normal;}
    .tg th{border-color:black;border-style:solid;border-width:0px;font-family:Helvetica, sans-serif;font-size:24px;
      font-weight:normal;overflow:hidden;padding:5px 5px;word-break:normal;}
    .tg .tg-vxqb1{ background-color: lightblue; color: white; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}
    .tg .tg-vxqb2{ background-color: pink; color: white; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}
    img { max-width: 100%; height: auto;}
    .pink {color: pink; font-weight: normal; font-size:16px;}
    .pink_bold {color: pink; font-weight: bold;} 
    .green_bold {color: green; font-weight: bold;} 
    .lightblue {color: lightblue; font-weight: normal; font-size:16px;}
    .lightblue_bold {color: lightblue; font-weight: bold;}
    .lightseagreen {color: lightseagreen; font-weight: normal; font-size:16px;}
    .MediumSlateBlue {color: MediumSlateBlue; font-weight: normal; font-size:16px;}
    
    .tg .tg-vxqb3{font-weight: bold; font-size:18px; background-color: lightseagreen; color: white; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}
    .tg .tg-vxqb4{font-weight: bold; font-size:18px;background-color: MediumSlateBlue; color: white; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}

    .tg1 td{border: 3px solid black;}
    .tg1 th{border: 3px solid black;}
    .tg1 {border-collapse: collapse; border-style: hidden;}

    .tg .tg-vxqb6{ background-color: black; color: yellow; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}
    .tg .tg-vxqb7{ background-color: yellow; color: black; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}

    

    </style>
</head>
<body>

    <h1>
        <table class="tg" width="60%" align="center">
            <thead>
              <tr>                  
                  <th class="tg-vxqb1"><a href="index.php">HOMEPAGE</a></th>
                  <th class="tg-vxqb2"><a href="team.php">TEAM</a></th>
                  <th class="tg-vxqb1"><a href="classical.php">CLASSICAL&nbsp;♫</a></th>
                  <th class="tg-vxqb2"><a href="rock.php">ROCK&nbsp;♫</a></th>
                  <th class="tg-vxqb1"><a href="jazz.php">JAZZ&nbsp;♫</a></th>                 
              </tr>
            </thead>
            <tbody>              
                                          
            </tbody>
        </table>
        <table class="tg" width="60%" align="center">
            <thead>
              <tr>                  
                  <th class="tg-vxqb2"><a href="poster.php">POSTER</a></th>                  
                  <th class="tg-vxqb1"><a href="sponsors.php">SPONSORS</a></th>
                  <th class="tg-vxqb2"><a href="partners.php">PARTNERS</a></th>                    
                  <th class="tg-vxqb1"><a href="http://eth.app.cern.ch/UAhelp.php">HELP4UA</a></th>
                  <th class="tg-vxqb2"><a href="http://ukraineaidngo.app.cern.ch/donate.php">DONATE</a></th>
                                  
              </tr>
            </thead>

            <tbody>              
                                          
            </tbody>
        </table>
        </h1>

        <br /> <h1> &#119070; VENUE &#119070; </h1>
        <div align ="center">
        The venue of choice is <a href="https://www.geneva-arena.ch/"><span style="color:pink"><b>ARENA de GENEVE</b></span></a>.
        <br />
        </div>
        <br /> <h1> &#119070; DATE &#119070; </h1>        
        <div>
        We narrowed down the date choice to 3 possibilities:
        <br />
        <br />18th of June 2022
        <br /><b>25th of June 2022 - our strong preference </b>
        <br />2nd of July 2022
        <br />
        <br /> The date will be adjusted to the bands' availability.
        <br /> Final choice will be announced before the 15th of May.
        <br />        
      </div>
      
      <h1> 
        <br />&#119070; LINE-UP &#119070;
        <br /> 
        <br />&#9733; THE STAR of the EVENING &#9733;
        <br />
        <table class="tg" width="60%" align="center">
            <thead>
                <tr>    
                  <th class="tg-vxqb6"> - - - - _ - - - - - </th>                  
                  <th class="tg-vxqb7">&#129074;</th>  
                  <th class="tg-vxqb6">IN CONTACT</th>      
                </tr>
               <tr>    
                  <th class="tg-vxqb7"> - - - - - _ - - - - - - - </th>                  
                  <th class="tg-vxqb6">&#129074;</th>  
                  <th class="tg-vxqb7">IN CONTACT</th>      
              </tr>
                <tr>    
                  <th class="tg-vxqb6"> - - _ - - - - - - - </th>                  
                  <th class="tg-vxqb7">&#129074;</th>  
                  <th class="tg-vxqb6">IN CONTACT</th>      
                </tr>
               <tr>    
                  <th class="tg-vxqb7"> - - - - _ - - - - - - - </th>                  
                  <th class="tg-vxqb6">&#129074;</th>  
                  <th class="tg-vxqb7">IN CONTACT</th>      
              </tr>
              <tr>    
                  <th class="tg-vxqb6"> - - - - - - - _ - - - _ - - - - </th>                  
                  <th class="tg-vxqb7">&#129074;</th>  
                  <th class="tg-vxqb6">IN CONTACT</th>      
              </tr>
              <tr>    
                  <th class="tg-vxqb7"> - - - - - - _ - - - - - - - </th>                  
                  <th class="tg-vxqb6">&#129074;</th>  
                  <th class="tg-vxqb7">IN CONTACT</th>      
              </tr>
              <tr>    
                  <th class="tg-vxqb6"> - - - _ - - - - - - - - -</th>                  
                  <th class="tg-vxqb7">&#129074;</th>  
                  <th class="tg-vxqb6">IN CONTACT</th>      
              </tr>
            </thead>
            <tbody>
            <tr>
            </tr>
            </tbody>
        </table>

        <br />
        <br />&#9734; STARs of the EVENT &#9734;
        <br />
        
        <table class="tg" width="60%" align="center">
            <thead>
              
            </thead>
          
            <tbody>
            <tr>              
                  <td class="tg-vxqb4"><a href="https://haneketwins.com/" class="thick">Haneke Twins [Geneva/Zurich]</a></td>                  
                  <td class="tg-vxqb3"><a href="https://www.youtube.com/watch?v=9ypVmN4fQWo" class="thick">YOUTUBE</a></td>  
                  <td class="tg-vxqb4"><span class="confirmed">CONFIRMED!</span></td>
            </tr> 
            <tr>              
                  <td class="tg-vxqb3"><a href="http://www.useless-band.com/" class="thick">Useless [Lausanne]</a></td>                  
                  <td class="tg-vxqb4"><a href="https://www.youtube.com/watch?v=uT3cVMzz6FM" class="thick">YOUTUBE</a></td>  
                  <td class="tg-vxqb3"><span class="confirmed">CONFIRMED!</span></td>
            </tr>  
            <tr>              
                  <td class="tg-vxqb4"><a href="https://www.afterlite.ch/" class="thick">Afterlite [Lausanne]</a></td>                  
                  <td class="tg-vxqb3"><a href="https://soundcloud.com/user-888327899/when-you-go-out" class="thick">SOUNDCLOUD</a></td>  
                  <td class="tg-vxqb4"><span class="confirmed">CONFIRMED!</span></td>
            </tr> 
            <tr>              
                  <td class="tg-vxqb3"> - - - - - - - _ - - - - </td>                  
                  <td class="tg-vxqb4">&#129074;</td>  
                  <td class="tg-vxqb3"><span class="confirmed">IN CONTACT!</span></td>
            </tr>             
            </tbody>
        </table> 

        </h1>
        <br /> <h1> &#119070; STAGE &#119070; </h1>
        <div align ="center">
        We have prepared a <a href="stage.php"><span style="color:pink"><b>STAGE SETUP</b></span></a> for each band.
        <br />
        </div>
        
        <h1>
        <br />&#9734; <b>POLISH HOSPITALITY</b> &#9734;
        </h1>
        <div align ="center">        
        Hi! During the event I would like to show a bit of polish hospitality. All drinks and snacks will be distributed by our volunteers free of charge. <span class="lightblue_bold">YOU</span> may like to consider experiencing more of the polish hospitality by spending holidays in Poland this year. That would greatly support our economy in these difficult times. If <span class="lightblue_bold">YOU</span> do not return 2&nbsp;kg heavier then <span class="lightblue_bold">YOU</span> probably did not meet the right people!       
        
        </div>
        
        <div class="div2">
        <br />- Tomasz Gadek<br /> Event Coordinator
        </div>


        <br /> <h1> 	&#119070; TICKETS &#119070; </h1>
        <div align ="center">
        The tickets will be reasonably priced, accordingly to the costs of the venue, bands and organization, most probably: 
        <br /><br />
        <table class="tg1" width="100%" align="center">
            <thead>
                <tr>    
                  <th >BASIC</th>                  
                  <th >56.78 CHF</th>  
                  <th >[up to 4500 places]</th>      
                </tr>
                <tr>    
                  <th >SEAT</th>                  
                  <th >67.89 CHF</th>  
                  <th >[up to 2700 places]</th>      
                </tr>
               <tr>    
                  <th >VIP</th>                  
                  <th >123.45 CHF</th>  
                  <th >[up to 700 places]</th>      
              </tr>
              <tr>    
                  <th >VIP+Experience</th>                  
                  <th >234.56 CHF</th>  
                  <th >[exactly 103 places]</th>      
              </tr>
            </thead>
        </table>
        
        
        <br /><span class="MediumSlateBlue">The Experience = a souvenir + a seat at Loges VIP + an extra event, e.g. meeting the bands.</span>
        <br /><span class="lightseagreen">VIP guarantees well located seats in the balconies.</span>
        <br />
        <br />Re-selling or refund of the tickets will NOT be possible!
        <br />
        <br /> The reason for cheap tickets is that around 20% of their price is lost in venue and ticketing fees. We prefer that <span class="lightblue_bold">YOU</span> save on the ticket price and, if you liked the concert, donate whatever you feel like afterwards. Of course, you can also donate before the concert, or right away! We count on <span class="lightblue_bold">YOUR GOOD HEART</span> and donations directly to the <a href="http://ukraineaidngo.app.cern.ch/"><span class="green_bold">Ukraine Aid</span></a> association account or other NGOs of <span class="lightblue_bold">YOUR CHOICE</span>. For organizational reasons we will not be able to accept/collect cash donations during the Rock Concert, we encourage online donations. 
        <br />
        <br /> <img position="absolute" src="logo_concert.png" alt="Concert Logo" width="60%" height="60%" align="center">
    
</div>    
</body>
<footer width="60%" align = "right">
    <foot width="60%">     
    <br /> powered by Tomasz Gadek & coffee <br />
    <p><img src="https://www.darmowylicznik.pl/licznik.php?id=145321" alt="Hits counter" style="border:1px;" /></p>
    </foot>
</footer>
</html>

