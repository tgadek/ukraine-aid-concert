<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ukraine Aid Concert</title>
    <link rel = "icon" href = "logo_concert_mini.png" type = "image/x-icon">
    <style type="text/css">
    h1 {text-align: center; font-family: Helvetica, sans-serif; font-size: 24px;}
    p {text-align: right;}
    A {text-decoration: none; color: white; font-weight: bold;}
    a {text-decoration: none; color: white; font-weight: bold;}
    div {text-align: center; align: center; font-family: Helvetica, sans-serif; font-size: 18px; width: 60%; align-items: center; justify-content: center; margin: auto;}
    .div2 {text-align: right; align: center; font-family: Helvetica, sans-serif; font-size: 18px; width: 60%; align-items: center; justify-content: center; margin: auto;}
    info {text-align: center; font-family: Helvetica, sans-serif; font-size: 15px;}
    foot {text-align: right; font-family:"Helvetica", Helvetica, sans-serif; font-size:10px;}
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{border-color:black;border-style:solid;border-width:0px;font-family:Helvetica, sans-serif;font-size:18px;
      overflow:hidden;padding:5px 5px;word-break:normal;}
    .tg th{border-color:black;border-style:solid;border-width:0px;font-family:Helvetica, sans-serif;font-size:24px;
      font-weight:normal;overflow:hidden;padding:5px 5px;word-break:normal;}
    .tg .tg-vxqb1{ background-color: lightblue; color: white; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}
    .tg .tg-vxqb2{ background-color: pink; color: white; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}
    img { max-width: 100%; height: auto;}
    .pink_bold {color: pink; font-weight: bold;} 
    .lightblue_bold {color: lightblue; font-weight: bold;} 
    </style>
</head>
<body>

    <h1>
        <table class="tg" width="60%" align="center">
            <thead>
              <tr>                  
                  <th class="tg-vxqb1"><a href="index.php">HOMEPAGE</a></th>
                  <th class="tg-vxqb2"><a href="team.php">TEAM</a></th>
                  <th class="tg-vxqb1"><a href="classical.php">CLASSICAL&nbsp;♫</a></th>
                  <th class="tg-vxqb2"><a href="rock.php">ROCK&nbsp;♫</a></th>
                  <th class="tg-vxqb1"><a href="jazz.php">JAZZ&nbsp;♫</a></th>                 
              </tr>
            </thead>
            <tbody>              
                                          
            </tbody>
        </table>
        <table class="tg" width="60%" align="center">
            <thead>
              <tr>                  
                  <th class="tg-vxqb2"><a href="poster.php">POSTER</a></th>                  
                  <th class="tg-vxqb1"><a href="sponsors.php">SPONSORS</a></th>
                  <th class="tg-vxqb2"><a href="partners.php">PARTNERS</a></th>                    
                  <th class="tg-vxqb1"><a href="http://eth.app.cern.ch/UAhelp.php">HELP4UA</a></th>
                  <th class="tg-vxqb2"><a href="http://ukraineaidngo.app.cern.ch/donate.php">DONATE</a></th>
                                  
              </tr>
            </thead>

            <tbody>              
                                          
            </tbody>
        </table>
        </h1>
        <div align ="center">
        <br />Currently we are negotiating with several sponsors.
        <br />
        <br />Maybe <span class="lightblue_bold">YOU</span> could help us finding one more?
        <br />
        <br />Contact us via the <a href="team.php"><span class="pink_bold">TEAM</span></a> sub-page.
        <br />
        <br />
        <br />
        <img position="absolute" src="logo_concert.png" alt="Concert Logo" width="50%" height="50%" align="center">
        </div>
    
</body>
<footer align = "right">
    <foot> 
    <br /> powered by Tomasz Gadek & coffee <br />
    <p><img src="https://www.darmowylicznik.pl/licznik.php?id=145321" alt="Hits counter" style="border:1px;" /></p>

    </foot>
</footer>
</html>
