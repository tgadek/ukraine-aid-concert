<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ukraine Aid Concert</title>
    <link rel = "icon" href = "logo_concert_mini.png" type = "image/x-icon">
    <style type="text/css">
    h1 {text-align: center; font-family: Helvetica, sans-serif; font-size: 24px;}
    
    A {text-decoration: none; color: white; font-weight: bold;}
    a {text-decoration: none; color: white; font-weight: bold;}
    div {text-align: center; align: center; font-family: Helvetica, sans-serif; font-size: 18px; width: 60%; align-items: center; justify-content: center; margin: auto;}
    .div2 {text-align: right; align: center; font-family: Helvetica, sans-serif; font-size: 18px; width: 60%; align-items: center; justify-content: center; margin: auto;}
     p {text-align: right;}
    info {text-align: center; font-family: Helvetica, sans-serif; font-size: 15px;}
    foot {text-align: right; font-family:"Helvetica", Helvetica, sans-serif; font-size:10px;}
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{border-color:black;border-style:solid;border-width:0px;font-family:Helvetica, sans-serif;font-size:18px;
      overflow:hidden;padding:5px 5px;word-break:normal;}
    .tg th{border-color:black;border-style:solid;border-width:0px;font-family:Helvetica, sans-serif;font-size:24px;
      font-weight:normal;overflow:hidden;padding:5px 5px;word-break:normal;}
    .tg .tg-vxqb1{ background-color: lightblue; color: white; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}
    .tg .tg-vxqb2{ background-color: pink; color: white; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}
    img { max-width: 100%; height: auto;}
    .pink {color: pink; font-weight: normal; font-size:16px;}
    .pink_bold {color: pink; font-weight: bold;} 
    .green_bold {color: green; font-weight: bold;} 
    .lightblue {color: lightblue; font-weight: normal; font-size:16px;}
    .lightblue_bold {color: lightblue; font-weight: bold;}
    .lightseagreen {color: lightseagreen; font-weight: normal; font-size:16px;}
    .MediumSlateBlue {color: MediumSlateBlue; font-weight: normal; font-size:16px;}
    
    .tg .tg-vxqb3{font-weight: bold; font-size:18px; background-color: lightseagreen; color: white; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}
    .tg .tg-vxqb4{font-weight: bold; font-size:18px;background-color: MediumSlateBlue; color: white; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}

    .tg1 td{border: 3px solid black;}
    .tg1 th{border: 3px solid black;}
    .tg1 {border-collapse: collapse; border-style: hidden;}

    .tg .tg-vxqb6{ background-color: black; color: yellow; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}
    .tg .tg-vxqb7{ background-color: yellow; color: black; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}

    </style>
</head>
<body>

    <h1>
        <table class="tg" width="60%" align="center">
            <thead>
              <tr>                  
                  <th class="tg-vxqb1"><a href="index.php">HOMEPAGE</a></th>
                  <th class="tg-vxqb2"><a href="team.php">TEAM</a></th>
                  <th class="tg-vxqb1"><a href="classical.php">CLASSICAL&nbsp;♫</a></th>
                  <th class="tg-vxqb2"><a href="rock.php">ROCK&nbsp;♫</a></th>
                  <th class="tg-vxqb1"><a href="jazz.php">JAZZ&nbsp;♫</a></th>                 
              </tr>
            </thead>
            <tbody>              
                                          
            </tbody>
        </table>
        <table class="tg" width="60%" align="center">
            <thead>
              <tr>                  
                  <th class="tg-vxqb2"><a href="poster.php">POSTER</a></th>                  
                  <th class="tg-vxqb1"><a href="sponsors.php">SPONSORS</a></th>
                  <th class="tg-vxqb2"><a href="partners.php">PARTNERS</a></th>                    
                  <th class="tg-vxqb1"><a href="http://eth.app.cern.ch/UAhelp.php">HELP4UA</a></th>
                  <th class="tg-vxqb2"><a href="http://ukraineaidngo.app.cern.ch/donate.php">DONATE</a></th>
                                  
              </tr>
            </thead>

            <tbody>              
                                          
            </tbody>
        </table>
        </h1>

        <br /> <h1> &#119070; VENUE &#119070; </h1>
        <div align ="center">
        in GENEVA or MONTRUEX
        <br />
        </div>
        <br /> <h1> &#119070; DATE &#119070; </h1>        
        <div>
       <br /> AUTUMN 2022      
      </div>
      
      <h1> 
        <br />&#119070; LINE-UP &#119070;
        <br /> 
        <br />&#9733; THE STAR of the EVENING &#9733;
        <br />
        <table class="tg" width="60%" align="center">
            <thead>
                <tr>    
                  <th class="tg-vxqb6"> - - - - - </th>                  
                  <th class="tg-vxqb7">&#129074;</th>  
                  <th class="tg-vxqb6">IN CONTACT</th>      
                </tr>
               <tr>    
                  <th class="tg-vxqb7"> - - - - - </th>                  
                  <th class="tg-vxqb6">&#129074;</th>  
                  <th class="tg-vxqb7">IN CONTACT</th>      
              </tr>

            </thead>
            <tbody>
            <tr>
            </tr>
            </tbody>
        </table>

        <br />
        <br />&#9734; STARs of the EVENT &#9734;
        <br />
        
        <table class="tg" width="60%" align="center">
            <thead>
              
            </thead>
          
            <tbody>
            <tr>              
                  <td class="tg-vxqb4"><a href="jazz.php" class="thick"> - - - - - [- - -]</a></td>                  
                  <td class="tg-vxqb3">&#129074;</td>  
                  <td class="tg-vxqb4"><span class="confirmed">IN CONTACT!</span></td>
            </tr> 
            <tr>              
                  <td class="tg-vxqb3"><a href="jazz.php" class="thick"> - - - - - [- - -]</a></td>                     
                  <td class="tg-vxqb4">&#129074;</td>  
                  <td class="tg-vxqb3"><span class="confirmed">IN CONTACT!</span></td>
            </tr>          
            </tbody>
        </table>
    </h1>


        <br /> <h1> 	&#119070; TICKETS &#119070; </h1>
        <div align ="center">
        The tickets will be reasonably priced, accordingly to the costs of the venue, bands and organization, most probably: 
        <br /><br />
        <table class="tg1" width="100%" align="center">
            <thead>
                <tr>    
                  <th >BASIC</th>                  
                  <th >- - CHF</th>  
                  <th >[up to - - - places]</th>      
                </tr>
                <tr>    
                  <th >SEAT</th>                  
                  <th >- - CHF</th>  
                  <th >[up to - - - places]</th>      
                </tr>
               <tr>    
                  <th >VIP</th>                  
                  <th > - - CHF</th>  
                  <th >[up to - - - places]</th>      
              </tr>
              <tr>    
                  <th >VIP+Experience</th>                  
                  <th > - - CHF</th>  
                  <th >[exactly - - - places]</th>      
              </tr>
            </thead>
        </table>
        
        
        <br /><span class="MediumSlateBlue">The Experience = a souvenir + VIP seat + an extra event, e.g. meeting the bands.</span>
        <br /><span class="lightseagreen">VIP guarantees well located seats.</span>
        <br />
        <br />Re-selling or refund of the tickets will NOT be possible!
        <br />
        <br /> The reason for cheap tickets is that around 20% of their price is lost in venue and ticketing fees. We prefer that <span class="lightblue_bold">YOU</span> save on the ticket price and, if you liked the concert, donate whatever you feel like afterwards. Of course, you can also donate before the concert, or right away! We count on <span class="lightblue_bold">YOUR GOOD HEART</span> and donations directly to the <a href="http://ukraineaidngo.app.cern.ch/"><span class="green_bold">Ukraine Aid</span></a> association account or other NGOs of <span class="lightblue_bold">YOUR CHOICE</span>. For organizational reasons we will not be able to accept/collect cash donations during the Rock Concert, we encourage online donations. 
        <br />
        <br /> <img position="absolute" src="logo_concert.png" alt="Concert Logo" width="60%" height="60%" align="center">
    
</div>    
</body>
<footer width="60%" align = "right">
    <foot width="60%">     
    <br /> powered by Tomasz Gadek & coffee <br />
    <p><img src="https://www.darmowylicznik.pl/licznik.php?id=145321" alt="Hits counter" style="border:1px;" /></p>
    </foot>
</footer>
</html>

