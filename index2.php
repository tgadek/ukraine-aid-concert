<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ukraine Aid Concert</title>
    <link rel = "icon" href = "logo_concert_mini.png" type = "image/x-icon">
    <style type="text/css">
    h1 {text-align: center; font-family: Helvetica, sans-serif;}
    p {text-align: right;}
    A {text-decoration: none; color: white; font-weight: bold;}
    a {text-decoration: none; color: white; font-weight: bold;}
    div {text-align: center; align: center; font-family: Helvetica, sans-serif; font-size: 18px; width: 60%; align-items: center; justify-content: center; margin: auto;}
    .div2 {text-align: right; align: center; font-family: Helvetica, sans-serif; font-size: 18px; width: 60%; align-items: center; justify-content: center; margin: auto;}
    info {text-align: center; font-family: Helvetica, sans-serif; font-size: 15px;}
    foot {text-align: right; font-family:"Helvetica", Helvetica, sans-serif; font-size:10px;}
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{border-color:black;border-style:solid;border-width:0px;font-family:Helvetica, sans-serif;font-size:18px;
      overflow:hidden;padding:5px 5px;word-break:normal;}
    .tg th{border-color:black;border-style:solid;border-width:0px;font-family:Helvetica, sans-serif;font-size:24px;
      font-weight:normal;overflow:hidden;padding:5px 5px;word-break:normal;}
    .tg .tg-vxqb1{ background-color: lightblue; color: white; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}
    .tg .tg-vxqb2{ background-color: pink; color: white; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}
    img { max-width: 900px; height: auto;}
    .green_bold {color: green; font-weight: bold;} 
    .pink_bold {color: pink; font-weight: bold;} 
    .lightblue_bold {color: lightblue; font-weight: bold;} 

    </style>
</head>
<body>

    <h1>
        <table class="tg" width="60%" align="center">
            <thead>
              <tr>                  
                  <th class="tg-vxqb1"><a href="index.php">HOMEPAGE</a></th>
                  <th class="tg-vxqb2"><a href="team.php">TEAM</a></th>
                  <th class="tg-vxqb1"><a href="classical.php">CLASSICAL&nbsp;♫</a></th>
                  <th class="tg-vxqb2"><a href="rock.php">ROCK&nbsp;♫</a></th>
                  <th class="tg-vxqb1"><a href="jazz.php">JAZZ&nbsp;♫</a></th>                 
              </tr>
            </thead>
            <tbody>              
                                          
            </tbody>
        </table>
        <table class="tg" width="60%" align="center">
            <thead>
              <tr>                  
                  <th class="tg-vxqb2"><a href="poster.php">POSTER</a></th>                  
                  <th class="tg-vxqb1"><a href="sponsors.php">SPONSORS</a></th>
                  <th class="tg-vxqb2"><a href="partners.php">PARTNERS</a></th>                    
                  <th class="tg-vxqb1"><a href="http://eth.app.cern.ch/UAhelp.php">HELP4UA</a></th>
                  <th class="tg-vxqb2"><a href="http://ukraineaidngo.app.cern.ch/donate.php">DONATE</a></th>
                                  
              </tr>
            </thead>

            <tbody>              
                                          
            </tbody>
        </table>
        
        </h1>
        <div align ="center">
        <br />&#9734; <b>MISSION</b> &#9734;
        <br />
        <br />Hi! We are a team of music enthusiasts working at various companies and institutes, such as <b>ETH Zurich</b> or <b>AGH Cracow</b> and we are mostly based in Geneva region. We are organizing a series of charity concerts to raise funds for  humanitarian aid in Ukraine via our associations. One of them, <a href="http://ukraineaidngo.app.cern.ch/"><span class="green_bold">Ukraine Aid</span></a>,  helps the Victims of War in Ukraine directly with minimum bureaucracy and next to zero costs of running! We have observed a drastic loss of interest in donations for the issue that is not resolved yet. We would like to raise awareness and offer <span class="lightblue_bold">YOU</span> a bit of entertainment in exchange for <span class="lightblue_bold">YOUR</span> financial support. We cordially invite you to participate in our concerts.</div>
        <div class="div2">
        <br />- Tomasz Gadek<br /> Event Coordinator
        </div>
        <div align ="center">
        <br />
        <br />&#9734; <b>LOCATION</b> &#9734;
        <br />
        <br />We are sure that you will enjoy the location of the <b>Ukraine Aid Concerts</b>. Geneva is a beautiful city with many charming lake views and famous sites to visit. On top of that We have booked superb venues for our concerts. The Classical Music Concert will be hosted in the stunning <b>Victoria Hall in Geneva</b>. The Rock Concert will take place in the <b>ARENA de Geneve</b>, which can serve up to 9,500 people! <b>Arena</b> is also conviniently located right next to the airport and a train station! We are counting on your presence!
<br />
</div>
<h1>
    <img position="absolute" src="logo_concert.png" alt="Concert Logo" width="50%" height="50%" align="center">
</h1>
    
</body>
<footer width="60%" align = "right">
    <foot width="60%"> 
    
    <br /> powered by Tomasz Gadek & coffee <br />
    <p><img src="https://www.darmowylicznik.pl/licznik.php?id=145321" alt="Hits counter" style="border:1px;" /></p>
    
    </foot>
</footer>
</html>
