<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ukraine Aid Concert</title>
    <link rel = "icon" href = "logo_concert_mini.png" type = "image/x-icon">
    <style type="text/css">
    h1 {text-align: center; font-family: Helvetica, sans-serif;}
    p {text-align: center; margin: auto; width: 100%;}
    A {text-decoration: none; color: white; font-weight: bold;}
    a {text-decoration: none; color: white; font-weight: bold;}
    div {text-align: right; font-family: Helvetica, sans-serif; font-size: 30px;}
    info {text-align: center; font-family: Helvetica, sans-serif; font-size: 15px;}
    foot {text-align: right; font-family:"Helvetica", Helvetica, sans-serif; font-size:10px;}
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{border-color:black;border-style:solid;border-width:0px;font-family:Helvetica, sans-serif;font-size:18px;
      overflow:hidden;padding:5px 5px;word-break:normal;}
    .tg th{border-color:black;border-style:solid;border-width:0px;font-family:Helvetica, sans-serif;font-size:24px;
      font-weight:normal;overflow:hidden;padding:5px 5px;word-break:normal;}
    .tg .tg-vxqb1{ background-color: lightblue; color: white; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center;}
    .tg .tg-vxqb2{ background-color: pink; color: white; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center;}
    img { max-width: 1600px; height: auto; text-align: center; margin: auto;}
    </style>
</head>
<body>

    <h1>
        <table class="tg" width="60%" align="center">
            <thead>
              <tr>                  
                  <th class="tg-vxqb1"><a href="index.php">HOMEPAGE</a></th>
                  <th class="tg-vxqb2"><a href="team.php">TEAM</a></th>
                  <th class="tg-vxqb1"><a href="classical.php">CLASSICAL&nbsp;♫</a></th>
                  <th class="tg-vxqb2"><a href="rock.php">ROCK&nbsp;♫</a></th>
                  <th class="tg-vxqb1"><a href="jazz.php">JAZZ&nbsp;♫</a></th>                 
              </tr>
            </thead>
            <tbody>              
                                          
            </tbody>
        </table>
        <table class="tg" width="60%" align="center">
            <thead>
              <tr>                  
                  <th class="tg-vxqb2"><a href="poster.php">POSTER</a></th>                  
                  <th class="tg-vxqb1"><a href="sponsors.php">SPONSORS</a></th>
                  <th class="tg-vxqb2"><a href="partners.php">PARTNERS</a></th>                    
                  <th class="tg-vxqb1"><a href="http://eth.app.cern.ch/UAhelp.php">HELP4UA</a></th>
                  <th class="tg-vxqb2"><a href="http://ukraineaidngo.app.cern.ch/donate.php">DONATE</a></th>
                                  
              </tr>
            </thead>

            <tbody>              
                                          
            </tbody>
        </table>
        <br />
        STAGE FOR <a href="http://www.useless-band.com/" class="thick"><span style="color: pink">Useless [Lausanne]</span></a>
        </h1>
        <p><img src="stage/stage_ul.PNG" alt="stage_ul" width="100%" align="center"></p>
        <h1>
        <br />
        STAGE FOR <a href="https://www.afterlite.ch/" class="thick"><span style="color: pink">Afterlite [Lausanne]</span></a>
        </h1>
        <p><img src="stage/stage_al.PNG" alt="stage_al" width="100%" align="center"></p>
        <h1>
        <br />
        STAGE FOR <a href="https://haneketwins.com/" class="thick"><span style="color: pink">Haneke Twins [Geneva/Zurich]</span></a>
        </h1>
        <p><img src="stage/stage_ht.PNG" alt="stage_ht" width="100%" align="center"></p>
        <h1>
        <br />
        STAGE FOR <a href="stage.php" class="thick"><span style="color: pink">the Big Band</span></a>
        </h1>
        <p><img  src="stage/stage_r1.PNG" alt="stage_r1" width="100%" align="center"></p>
        <h1>
        <br />
        STAGE FOR <a href="stage.php" class="thick"><span style="color: pink">the STAR of the EVENING</span></a>
        </h1>
        <p><img src="stage/stage_at.PNG" alt="stage_at" width="100%" align="center"></p>
        <h1>
        <br />
        STAGE DIMENSIONS and TECHNICAL INFO
        </h1>
        <p><img  src="stage/stage.PNG" alt="stage" width="100%" align="center"></p>
           
    
</body>
<footer align = "right">
    <foot> 
    <br /> powered by Tomasz Gadek & coffee <br />
    <div class="rotator-slide" id="rotator-slide-1"><img src="https://www.darmowylicznik.pl/licznik.php?id=145321" alt="Hits counter" style="border:1px;" /></div>

    </foot>
</footer>
</html>
