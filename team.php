<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ukraine Aid Concert</title>
    <link rel = "icon" href = "logo_concert_mini.png" type = "image/x-icon">
    <style type="text/css">
    h1 {text-align: center; font-family: Helvetica, sans-serif; font-size: 24px;}
    p {text-align: left;}
    A {text-decoration: none; color: white; font-weight: bold;}
    a {text-decoration: none; color: white; font-weight: bold;}
    div {text-align: right; font-family: Helvetica, sans-serif; font-size: 30px;}
    info {text-align: center; font-family: Helvetica, sans-serif; font-size: 15px;}
    foot {text-align: right; font-family:"Helvetica", Helvetica, sans-serif; font-size:10px;}
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{border-color:black;border-style:solid;border-width:0px;font-family:Helvetica, sans-serif;font-size:18px;
      overflow:hidden;padding:5px 5px;word-break:normal;}
    .tg th{border-color:black;border-style:solid;border-width:0px;font-family:Helvetica, sans-serif;font-size:24px;
      font-weight:normal;overflow:hidden;padding:5px 5px;word-break:normal;}
    .tg .tg-vxqb1{ background-color: lightblue; color: white; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}
    .tg .tg-vxqb2{ background-color: pink; color: white; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}
    .tg .tg-vxqb3{ background-color: MediumSlateBlue; color: white; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}
    .tg .tg-vxqb4{ background-color: lightseagreen; color: white; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}
    .img { max-width: 100%; height: 300px; width: auto;}
    .img2 { max-width: 100%; height: auto;}
    </style>
</head>
<body>

    <h1>
        <table class="tg" width="60%" align="center">
            <thead>
              <tr>                  
                  <th class="tg-vxqb1"><a href="index.php">HOMEPAGE</a></th>
                  <th class="tg-vxqb2"><a href="team.php">TEAM</a></th>
                  <th class="tg-vxqb1"><a href="classical.php">CLASSICAL&nbsp;♫</a></th>
                  <th class="tg-vxqb2"><a href="rock.php">ROCK&nbsp;♫</a></th>
                  <th class="tg-vxqb1"><a href="jazz.php">JAZZ&nbsp;♫</a></th>                 
              </tr>
            </thead>
            <tbody>              
                                          
            </tbody>
        </table>
        <table class="tg" width="60%" align="center">
            <thead>
              <tr>                  
                  <th class="tg-vxqb2"><a href="poster.php">POSTER</a></th>                  
                  <th class="tg-vxqb1"><a href="sponsors.php">SPONSORS</a></th>
                  <th class="tg-vxqb2"><a href="partners.php">PARTNERS</a></th>                    
                  <th class="tg-vxqb1"><a href="http://eth.app.cern.ch/UAhelp.php">HELP4UA</a></th>
                  <th class="tg-vxqb2"><a href="http://ukraineaidngo.app.cern.ch/donate.php">DONATE</a></th>
                                  
              </tr>
            </thead>

            <tbody>              
                                          
            </tbody>
        </table>
        <br /> Our Team
        <table class="tg" width="60%" align="center">
            <thead>
              <tr>    
                  <th ></th>                  
                  <th ></th>  
                  <th ></th>          
                                  
              </tr>
            </thead>

            <tbody>
            <tr>              
                  <td class="tg-vxqb3"><img position="absolute" src="people/tomasz.png" alt="Tomasz" height="200px" align="center"></td>                  
                  <td class="tg-vxqb4">Tomasz</td>  
                  <td class="tg-vxqb3">Event Coordinator, Bands and Sponsors coordinator, website, graphics, line-up, sailor
                  <br />
                  <br /> +48 517 six two nine 222
                  <br /> tomasz.gadek [at] cern.ch 
                  </td>
            </tr>    
            <tr>              
                  <td class="tg-vxqb4"><img position="absolute" src="people/ilaria.jpg" alt="Marina" height="200px" align="center"></td>                  
                  <td class="tg-vxqb3">Ilaria</td>  
                  <td class="tg-vxqb4">Co-Organizer, Swiss bands contact, sailor</td>
            </tr>
            <tr>              
                  <td class="tg-vxqb3"><img position="absolute" src="people/dorota.jpg " alt="Dorota" height="200px" align="center"></td>                 
                  <td class="tg-vxqb4">Dorota</td>  
                  <td class="tg-vxqb3">Co-Organizer, Gifts for Bands and Sponsors coordinator</td>
            </tr>
            <tr>              
                  <td class="tg-vxqb4"><img position="absolute" src="people/eric.jpg" alt="Marina" height="200px" align="center"></td>                  
                  <td class="tg-vxqb3">Eric</td>  
                  <td class="tg-vxqb4">Co-organizer, conductor of Harmonie Nautique, Victoria Hall contact, sailor </td>
            </tr>
            <tr>              
                  <td class="tg-vxqb3"><img position="absolute" src="people/aga.jpg" alt="Aga" height="200px" align="center"></td>                  
                  <td class="tg-vxqb4">Aga</td>  
                  <td class="tg-vxqb3">Co-Organizer, Media&Marketing Coordinator, first aider
                  <br/>
                  <br/> a.zago [at] ieee.org
                  
                  </td>
            </tr>
            <tr>              
                  <td class="tg-vxqb4"><img position="absolute" src="people/sarah.jpg" alt="Sarah" height="200px" align="center"></td>                  
                  <td class="tg-vxqb3">Sarah</td>  
                  <td class="tg-vxqb4">Co-Organizer, Volunteers and Gifts coordinator, CERN guide, sailor</td>
            </tr> 
            <tr>              
                  <td class="tg-vxqb3"><img position="absolute" src="people/michael.jpg" alt="Michael" height="200px" align="center"></td>                  
                  <td class="tg-vxqb4">Michael</td>  
                  <td class="tg-vxqb3">Co-organizer, Photographer, sailor, ex-pilot</td>
            </tr>              
           <tr>              
                  <td class="tg-vxqb4"><img position="absolute" src="people/ale.jpeg" alt="Ale" height="200px" align="center"></td>                  
                  <td class="tg-vxqb3">Ale</td>  
                  <td class="tg-vxqb4">Co-organizer, Hotels&Lodging coordinator
                  <br />
                  <br /> +41 76 two one zero 01 24
                  <br /> alessandro.caratelli [at] cern.ch
                  </td>
            </tr>
            <tr>              
                  <td class="tg-vxqb3"><img position="absolute" src="people/jochen.jpg" alt="Jochen" height="200px" align="center"></td>                  
                  <td class="tg-vxqb4">Jochen</td>  
                  <td class="tg-vxqb3">Co-organizer, Logistics coordinator, sailor, pilot</td>
            </tr> 
            <tr>              
                  <td class="tg-vxqb4"></td>                  
                  <td class="tg-vxqb3">Sascha</td>  
                  <td class="tg-vxqb4">Co-Organizer, contacts specialist, CERN guide, first aider, sailor, pilot</td>
            </tr> 
            <tr>              
                  <td class="tg-vxqb3"></td>                  
                  <td class="tg-vxqb4">Eva</td>  
                  <td class="tg-vxqb3">Co-Organizer, Gifts for Bands coordinator, sailor</td>
            </tr>
            <tr>              
                  <td class="tg-vxqb4"><img position="absolute" src="people/chris.jpg" alt="Chris" height="200px" align="center"></td>                  
                  <td class="tg-vxqb3">Chris</td>  
                  <td class="tg-vxqb4">Co-Organizer, T-shirts coordinator
                  <br />
                  <br /> +48 608 zero four four 440
                  <br /> krzysztof.stachon [at] cern.ch
                  </td>
            </tr> 
            <tr>              
                  <td class="tg-vxqb3"><img position="absolute" src="people/asia.jpg" alt="Asia" height="200px" align="center"></td>                  
                  <td class="tg-vxqb4">Asia</td>  
                  <td class="tg-vxqb3">Co-Organizer, Crowd control coordinator, CERN guide, first aider</td>
            </tr>
            <tr>              
                  <td class="tg-vxqb4"><img position="absolute" src="people/daria.jpg" alt="Olena" height="200px" align="center"></td>                  
                  <td class="tg-vxqb3">Daria</td>  
                  <td class="tg-vxqb4">Co-Organizer, native Ukrainian, first aider</td>
            </tr>   
            <tr>              
                  <td class="tg-vxqb3"><img position="absolute" src="people/olena.jpg" alt="Olena" height="200px" align="center"></td>                  
                  <td class="tg-vxqb4">Olena</td>  
                  <td class="tg-vxqb3">Co-Organizer, native Ukrainian</td>
            </tr>
            <tr>              
                  <td class="tg-vxqb4"></td>                  
                  <td class="tg-vxqb3">Anna</td>  
                  <td class="tg-vxqb4">Co-organizer, sailor</td>
            </tr>              
            <tr>              
                  <td class="tg-vxqb3"><img position="absolute" src="people/michal.jpg" alt="Michal" height="200px" align="center"></td>                  
                  <td class="tg-vxqb4">Michal</td>  
                  <td class="tg-vxqb3">Co-Organizer, Venue entrance coordinator</td>
            </tr> 
                  
            <tr>              
                  <td class="tg-vxqb4"></td>                  
                  <td class="tg-vxqb3">Oksana</td>  
                  <td class="tg-vxqb4">Volunteer, native Ukrainian, help in May</td>
            </tr> 
            
                         
            </tbody>
        </table>
    </h1>
    
</body>

<footer align = "right">
    <foot> 
    <br /> powered by Tomasz Gadek & coffee <br />
    <div class="rotator-slide" id="rotator-slide-1"><img class="img2" src="https://www.darmowylicznik.pl/licznik.php?id=145321" alt="Hits counter" style="border:1px;" /></div>

    </foot>
</footer>
</html>
