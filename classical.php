<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ukraine Aid Concert</title>
    <link rel = "icon" href = "logo_concert_mini.png" type = "image/x-icon">
    <style type="text/css">
    h1 {text-align: center; font-family: Helvetica, sans-serif; font-size: 24px;}
    
    A {text-decoration: none; color: white; font-weight: bold;}
    a {text-decoration: none; color: white; font-weight: bold;}
    div {text-align: center; align: center; font-family: Helvetica, sans-serif; font-size: 18px; width: 60%; align-items: center; justify-content: center; margin: auto;}
    .div2 {text-align: right; align: center; font-family: Helvetica, sans-serif; font-size: 18px; width: 60%; align-items: center; justify-content: center; margin: auto;}
     p {text-align: right;}
    info {text-align: center; font-family: Helvetica, sans-serif; font-size: 15px;}
    foot {text-align: right; font-family:"Helvetica", Helvetica, sans-serif; font-size:10px;}
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{border-color:black;border-style:solid;border-width:0px;font-family:Helvetica, sans-serif;font-size:18px;
      overflow:hidden;padding:5px 5px;word-break:normal;}
    .tg th{border-color:black;border-style:solid;border-width:0px;font-family:Helvetica, sans-serif;font-size:24px;
      font-weight:normal;overflow:hidden;padding:5px 5px;word-break:normal;}
    .tg .tg-vxqb1{ background-color: lightblue; color: white; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}
    .tg .tg-vxqb2{ background-color: pink; color: white; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}
    img { max-width: 100%; height: auto;}
    .pink {color: pink; font-weight: normal; font-size:16px;}
    .pink_bold {color: pink; font-weight: bold;} 
    .green_bold {color: green; font-weight: bold;} 
    .lightblue {color: lightblue; font-weight: normal; font-size:16px;}
    .lightblue_bold {color: lightblue; font-weight: bold;}
    .lightseagreen {color: lightseagreen; font-weight: normal; font-size:16px;}
    .MediumSlateBlue {color: MediumSlateBlue; font-weight: normal; font-size:16px;}
    
    .tg .tg-vxqb3{font-weight: bold; font-size:18px; background-color: lightseagreen; color: white; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}
    .tg .tg-vxqb4{font-weight: bold; font-size:18px;background-color: MediumSlateBlue; color: white; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}

    .tg1 td{border: 3px solid black;}
    .tg1 th{border: 3px solid black;}
    .tg1 {border-collapse: collapse; border-style: hidden;}

    .tg .tg-vxqb6{ background-color: black; color: yellow; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}
    .tg .tg-vxqb7{ background-color: yellow; color: black; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}

    

    </style>
</head>
<body>

    <h1>
        <table class="tg" width="60%" align="center">
            <thead>
              <tr>                  
                  <th class="tg-vxqb1"><a href="index.php">HOMEPAGE</a></th>
                  <th class="tg-vxqb2"><a href="team.php">TEAM</a></th>
                  <th class="tg-vxqb1"><a href="classical.php">CLASSICAL&nbsp;♫</a></th>
                  <th class="tg-vxqb2"><a href="rock.php">ROCK&nbsp;♫</a></th>
                  <th class="tg-vxqb1"><a href="jazz.php">JAZZ&nbsp;♫</a></th>                 
              </tr>
            </thead>
            <tbody>              
                                          
            </tbody>
        </table>
        <table class="tg" width="60%" align="center">
            <thead>
              <tr>                  
                  <th class="tg-vxqb2"><a href="poster.php">POSTER</a></th>                  
                  <th class="tg-vxqb1"><a href="sponsors.php">SPONSORS</a></th>
                  <th class="tg-vxqb2"><a href="partners.php">PARTNERS</a></th>                    
                  <th class="tg-vxqb1"><a href="http://eth.app.cern.ch/UAhelp.php">HELP4UA</a></th>
                  <th class="tg-vxqb2"><a href="http://ukraineaidngo.app.cern.ch/donate.php">DONATE</a></th>
                                  
              </tr>
            </thead>
            <tbody>              
                                          
            </tbody>
        </table>
        </h1>

        <br /> <h1> &#119070; VENUE &#119070; </h1>
        <div align ="center">
        The venue of choice is the stunning <a href="https://www.ville-ge.ch/culture/victoria_hall/"><span style="color:pink"><b>VICTORIA HALL in  GENEVA</b></span></a>.
        <br />
        </div>
        <br /> <h1> &#119070; DATE &#119070; </h1>
        <div align ="center">       
        <span style="color:black"><b>19th of JUNE 2022 at 19:00</b></span>
        <br /> 
        </div>   
      <h1> 
        <br />&#119070; LINE-UP &#119070;
      </h1>
      <div align ="center">
        <b>&#9733; THE STAR of the EVENING &#9733;</b>
        <br />with
        <br /><a href="https://www.harmonie-nautique.ch/" class="thick"><span style="color:pink; font-size: 24px">L'HARMONIE NAUTIQUE [Geneva]</span></a>
        <br/>
        </div>
        
        <h1> 	&#119070; TICKETS &#119070; </h1>
        <div align ="center">
        There will be a collection of cash donations before and after the Classical Music Concert. If you liked the concert, you can always donate online afterwards. Of course, you can also donate before the concert, or right away! We count on <span class="lightblue_bold">YOUR GOOD HEART</span> and donations directly to the <a href="http://ukraineaidngo.app.cern.ch/"><span class="green_bold">Ukraine Aid</span></a> association accounts or other NGOs of <span class="lightblue_bold">YOUR CHOICE</span>. 
        <br />
        <br /> <span class="lightblue_bold">YOU</span> can purchase <b>tickets</b> at:
        <br />
        <br />  <a href="https://billetterie-culture.geneve.ch/list/events" class="thick"><span style="color:pink">BILLETTERIE CULTURE DE GENEVE</span></a>
        <br />
        <br />
        <br /> <img position="absolute" src="logo_concert.png" alt="Concert Logo" width="60%" height="60%" align="center">
        </div>    
</body>
<footer width="60%" align = "right">
    <foot width="60%">     
    <br /> powered by Tomasz Gadek & coffee <br />
    <p><img src="https://www.darmowylicznik.pl/licznik.php?id=145321" alt="Hits counter" style="border:1px;" /></p>
    </foot>
</footer>
</html>

